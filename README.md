# alpine.me

inspiration at [Docker as IDE](https://medium.com/@ls12styler/docker-as-an-integrated-development-environment-95bc9b01d2c1)

## configure it

1. make a copy of `.env.sample` file as `.env`.
2. edit the value of `.env` file variables (and save it).

## build

`./build`

this gonna make the docker image tagged as `local/developer:${TAG}`.

> we recommend leave `TAG` as `latest` to make easier run your `devshell` command. [make it global](#make-it-global).

## make it global

`sudo ln -s ${PWD}/run /usr/local/bin/devshell`

## open devshell on your local project

`USERNAME=<USERNAME> devshell`

replace `<USERNAME>` with your `USERNAME` value of `.env` file.

> for more cleaner and short execution of your `devshell` command you can create an alias as `alias ds="USERNAME=<USERNAME> devshell"`, now you can run `ds` instead.
