FROM alpine:latest

ARG username
ARG password

ENV USER octo

# update and upgrade
RUN apk update \
    && apk upgrade

# install man pages packages
RUN apk add \
      man \
      man-pages \
      mdocml-apropos \
      less \
      less-doc

# ssh
RUN apk add openssh-client

# bash
RUN apk add bash bash-doc

# install tmux
RUN apk add tmux tmux-doc

# install git
RUN apk add git git-doc

# install editor
RUN apk add vim vim-doc

# docker, docker-compose
RUN apk add docker docker-compose

# curl
RUN apk add curl

# sudo
RUN apk add sudo
RUN echo "root ALL=(ALL) ALL" > /etc/sudoers
RUN echo "$username ALL=(ALL) ALL" >> /etc/sudoers

# create user
RUN echo -e "$password\n$password\n" | adduser $username
ENV HOME /home/$username

# set workspace as default
RUN mkdir /workspace
WORKDIR /workspace
RUN chown -R $username:$username /workspace

# clone vim base
RUN git clone --recursive https://gitlab.com/8topolar/vim.me.git ~/vim.me
RUN cd ~/vim.me && ./install

# set default user
USER $username

# default command
CMD ["tmux"]
